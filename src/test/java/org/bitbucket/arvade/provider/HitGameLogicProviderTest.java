package org.bitbucket.arvade.provider;

import org.bitbucket.arvade.service.gamelogic.impl.HitGameLogicStrategy;
import org.bitbucket.arvade.model.BlackjackCard;
import org.bitbucket.arvade.model.enums.Move;
import org.bitbucket.arvade.service.CardRandomizer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Java6Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class HitGameLogicProviderTest {

    @Mock
    private CardRandomizer<BlackjackCard> cardRandomizer;

    @InjectMocks
    private HitGameLogicProvider hitGameLogicStrategyProvider;

    private Move supportedMoveType = Move.HIT;

    @Test
    public void shouldReturnHitGameLogicStrategy() {
        //Given
        HitGameLogicStrategy expected = new HitGameLogicStrategy(cardRandomizer, supportedMoveType);

        //When
        HitGameLogicStrategy result = hitGameLogicStrategyProvider.get();

        //Then
        assertThat(result).isEqualToComparingFieldByField(expected);
    }
}
