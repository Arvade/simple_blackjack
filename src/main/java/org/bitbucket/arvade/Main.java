package org.bitbucket.arvade;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.bitbucket.arvade.factory.BlackjackDeckFactory;
import org.bitbucket.arvade.factory.GameFactory;
import org.bitbucket.arvade.factory.PlayerFactory;
import org.bitbucket.arvade.model.BlackjackDeck;
import org.bitbucket.arvade.model.Player;
import org.bitbucket.arvade.service.Game;

public class Main {

    public static void main(String[] args) {
        MainModule module = new MainModule();
        Injector injector = Guice.createInjector(module);

        GameFactory gameFactory = injector.getInstance(GameFactory.class);
        BlackjackDeckFactory blackjackDeckFactory = injector.getInstance(BlackjackDeckFactory.class);
        PlayerFactory playerFactory = injector.getInstance(PlayerFactory.class);

        Player player = playerFactory.createPlayer("John");
        Player dealer = playerFactory.createPlayer("Dealer", true);

        BlackjackDeck deck = blackjackDeckFactory.createDeck();

        Game game = gameFactory.createGame(player, dealer, deck);
        game.start();
    }
}
