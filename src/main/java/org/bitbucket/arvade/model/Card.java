package org.bitbucket.arvade.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.bitbucket.arvade.model.enums.CardSuit;

@Data
@AllArgsConstructor
public class Card {

    private CardSuit suit;

    private String rank;
}
