package org.bitbucket.arvade.service.decision;

import org.bitbucket.arvade.model.enums.Move;
import org.bitbucket.arvade.model.Player;

public interface DecisionTaker {

    Move getDecision(Player player, Player dealer, Player currentTurn);

    boolean supports(Player player, Player dealer, Player currentTurn);
}
