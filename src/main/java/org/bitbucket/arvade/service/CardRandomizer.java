package org.bitbucket.arvade.service;

import org.bitbucket.arvade.model.Card;

import java.util.List;

public interface CardRandomizer<T extends Card> {

    T randomizeCard(List<T> deck);
}
