package org.bitbucket.arvade.service.gamelogic;

import org.bitbucket.arvade.model.BlackjackCard;
import org.bitbucket.arvade.model.enums.Move;
import org.bitbucket.arvade.model.Player;
import org.bitbucket.arvade.model.enums.SpecialCase;

import java.util.List;

public interface GameLogic {

    SpecialCase process(Player currentTurn, List<BlackjackCard> deck, Move moveType);

    boolean supports(Move moveType);
}
