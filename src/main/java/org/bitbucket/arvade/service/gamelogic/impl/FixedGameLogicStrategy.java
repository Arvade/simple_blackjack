package org.bitbucket.arvade.service.gamelogic.impl;

import org.bitbucket.arvade.service.gamelogic.GameLogic;
import org.bitbucket.arvade.model.BlackjackCard;
import org.bitbucket.arvade.model.enums.Move;
import org.bitbucket.arvade.model.Player;
import org.bitbucket.arvade.model.enums.SpecialCase;

import java.util.List;

public class FixedGameLogicStrategy implements GameLogic {

    private SpecialCase specialCase;
    private boolean supports;

    public FixedGameLogicStrategy(SpecialCase specialCase, boolean supports) {
        this.specialCase = specialCase;
        this.supports = supports;
    }

    @Override
    public SpecialCase process(Player currentTurn, List<BlackjackCard> deck, Move moveType) {
        return this.specialCase;
    }

    @Override
    public boolean supports(Move moveType) {
        return this.supports;
    }
}
