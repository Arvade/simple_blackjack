package org.bitbucket.arvade.service;

import org.bitbucket.arvade.model.Player;

public interface StatisticsPrinter {

    void printStatistics(Player player, Player dealer);
}
