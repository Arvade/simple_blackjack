package org.bitbucket.arvade.service;

import org.bitbucket.arvade.model.BlackjackCard;

import java.util.List;

public interface BlackjackPointsCalculator {

    Integer calculatePoints(List<BlackjackCard> cards);
}
