package org.bitbucket.arvade.factory;

import com.google.inject.assistedinject.Assisted;
import org.bitbucket.arvade.service.Game;
import org.bitbucket.arvade.model.BlackjackDeck;
import org.bitbucket.arvade.model.Player;

public interface GameFactory {

    Game createGame(@Assisted(value = "player") Player player,
                    @Assisted(value = "dealer") Player dealer,
                    BlackjackDeck blackjackDeck);
}
