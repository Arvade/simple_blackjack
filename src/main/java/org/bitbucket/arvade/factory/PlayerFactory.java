package org.bitbucket.arvade.factory;

import com.google.inject.assistedinject.Assisted;
import org.bitbucket.arvade.model.Player;

public interface PlayerFactory {

    Player createPlayer(@Assisted String name);

    Player createPlayer(@Assisted String name, @Assisted boolean isDealer);
}
