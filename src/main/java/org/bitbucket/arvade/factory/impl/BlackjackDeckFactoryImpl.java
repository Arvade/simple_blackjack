package org.bitbucket.arvade.factory.impl;

import com.google.inject.Inject;
import org.bitbucket.arvade.factory.BlackjackCardFactory;
import org.bitbucket.arvade.factory.BlackjackDeckFactory;
import org.bitbucket.arvade.model.BlackjackDeck;
import org.bitbucket.arvade.model.enums.CardSuit;

public class BlackjackDeckFactoryImpl implements BlackjackDeckFactory {

    private static final String[] RANKS = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King", "Ace"};

    private BlackjackCardFactory blackjackCardFactory;

    @Inject
    public BlackjackDeckFactoryImpl(BlackjackCardFactory blackjackCardFactory) {
        this.blackjackCardFactory = blackjackCardFactory;
    }

    public BlackjackDeck createDeck() {
        BlackjackDeck deck = new BlackjackDeck();
        CardSuit[] cardSuits = CardSuit.values();

        for (String rank : RANKS) {
            for (CardSuit suit : cardSuits) {
                deck.addCard(blackjackCardFactory.createCard(suit, rank));
            }
        }

        return deck;
    }
}
