package org.bitbucket.arvade.factory.impl;

import org.bitbucket.arvade.factory.BlackjackCardFactory;
import org.bitbucket.arvade.model.BlackjackCard;
import org.bitbucket.arvade.model.enums.CardSuit;

public class BlackjackCardFactoryImpl implements BlackjackCardFactory {


    public BlackjackCard createCard(CardSuit suit, String rank) {
        Integer value;
        try {
            value = Integer.parseInt(rank);
        } catch (NumberFormatException e) {
            if (rank.equalsIgnoreCase("ace")) {
                value = 11;
            } else {
                value = 10;
            }
        }

        return new BlackjackCard(suit, rank, value);
    }
}
