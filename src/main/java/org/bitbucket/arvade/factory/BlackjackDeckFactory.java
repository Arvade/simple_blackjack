package org.bitbucket.arvade.factory;

import org.bitbucket.arvade.model.BlackjackDeck;

public interface BlackjackDeckFactory {

    BlackjackDeck createDeck();
}
