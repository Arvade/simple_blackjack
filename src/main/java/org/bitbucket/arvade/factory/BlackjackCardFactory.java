package org.bitbucket.arvade.factory;

import org.bitbucket.arvade.model.BlackjackCard;
import org.bitbucket.arvade.model.enums.CardSuit;

public interface BlackjackCardFactory {

    BlackjackCard createCard(CardSuit suit, String rank);
}
