package org.bitbucket.arvade;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import org.bitbucket.arvade.factory.BlackjackCardFactory;
import org.bitbucket.arvade.factory.BlackjackDeckFactory;
import org.bitbucket.arvade.factory.GameFactory;
import org.bitbucket.arvade.factory.PlayerFactory;
import org.bitbucket.arvade.factory.impl.BlackjackCardFactoryImpl;
import org.bitbucket.arvade.factory.impl.BlackjackDeckFactoryImpl;
import org.bitbucket.arvade.model.BlackjackCard;
import org.bitbucket.arvade.model.StatisticsTemplate;
import org.bitbucket.arvade.provider.*;
import org.bitbucket.arvade.service.BlackjackPointsCalculator;
import org.bitbucket.arvade.service.CardRandomizer;
import org.bitbucket.arvade.service.StatisticsPrinter;
import org.bitbucket.arvade.service.decision.DecisionTaker;
import org.bitbucket.arvade.service.gamelogic.GameLogic;
import org.bitbucket.arvade.service.gamelogic.impl.HitGameLogicStrategy;
import org.bitbucket.arvade.service.gamelogic.impl.StandGameLogicStrategy;
import org.bitbucket.arvade.service.impl.BlackjackCardRandomizer;
import org.bitbucket.arvade.service.impl.BlackjackPointsCalculatorImpl;
import org.bitbucket.arvade.service.impl.ConsoleStatisticsPrinter;

public class MainModule extends AbstractModule {

    protected void configure() {
        bind(BlackjackDeckFactory.class).to(BlackjackDeckFactoryImpl.class);
        bind(BlackjackCardFactory.class).to(BlackjackCardFactoryImpl.class);
        bind(BlackjackPointsCalculator.class).to(BlackjackPointsCalculatorImpl.class);
        bind(GameLogic.class).toProvider(CompositeGameLogicStrategyProvider.class);
        bind(new TypeLiteral<CardRandomizer<BlackjackCard>>() {
        })
                .to(new TypeLiteral<BlackjackCardRandomizer>() {
                });
        bind(DecisionTaker.class).toProvider(CompositeDecisionTakerProvider.class);
        bind(StatisticsPrinter.class).to(ConsoleStatisticsPrinter.class);
        bind(HitGameLogicStrategy.class).toProvider(HitGameLogicProvider.class);
        bind(StandGameLogicStrategy.class).toProvider(StandGameLogicProvider.class);
        install(new FactoryModuleBuilder().build(GameFactory.class));
        install(new FactoryModuleBuilder().build(PlayerFactory.class));
        bind(StatisticsTemplate.class).toProvider(StatisticsTemplateProvider.class);
    }
}
