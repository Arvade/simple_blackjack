package org.bitbucket.arvade.provider;

import com.google.inject.Inject;
import com.google.inject.Provider;
import lombok.Setter;
import org.bitbucket.arvade.service.gamelogic.impl.HitGameLogicStrategy;
import org.bitbucket.arvade.model.BlackjackCard;
import org.bitbucket.arvade.model.enums.Move;
import org.bitbucket.arvade.service.CardRandomizer;

public class HitGameLogicProvider implements Provider<HitGameLogicStrategy> {

    private static final Move SUPPORTED_MOVE_TYPE = Move.HIT;

    @Setter
    private Move supportedMoveType = SUPPORTED_MOVE_TYPE;

    private CardRandomizer<BlackjackCard> cardRandomizer;

    @Inject
    public HitGameLogicProvider(CardRandomizer<BlackjackCard> cardRandomizer) {
        this.cardRandomizer = cardRandomizer;
    }


    @Override
    public HitGameLogicStrategy get() {
        return new HitGameLogicStrategy(cardRandomizer, supportedMoveType);
    }
}
