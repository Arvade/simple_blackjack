package org.bitbucket.arvade.provider;

import com.google.inject.Provider;
import lombok.Setter;
import org.bitbucket.arvade.service.gamelogic.impl.StandGameLogicStrategy;
import org.bitbucket.arvade.model.enums.Move;

public class StandGameLogicProvider implements Provider<StandGameLogicStrategy> {

    private static final Move SUPPORTED_MOVE_TYPE = Move.STAND;

    @Setter
    private Move supportedMoveType = SUPPORTED_MOVE_TYPE;

    @Override
    public StandGameLogicStrategy get() {
        return new StandGameLogicStrategy(supportedMoveType);
    }
}
